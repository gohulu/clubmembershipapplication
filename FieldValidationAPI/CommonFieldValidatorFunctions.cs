﻿using System;
using System.Text.RegularExpressions;

namespace FieldValidationAPI
{
    public delegate bool RequiredValidDel(string fieldVal);
    public delegate bool StringLengValidDel(string fieldVal, int min, int max);
    public delegate bool DateValidDel(string fieldVal, out DateTime validDateTime);
    public delegate bool PatternMatchDel(string fieldVal, string pattern);
    public delegate bool CompareFieldsValidDel(string fieldVal, string fieldValCompate);

    public class CommonFieldValidatorFunctions
    {
        private static RequiredValidDel requiredValidDel = null;
        private static StringLengValidDel stringLengValidDel = null;
        private static DateValidDel dateValidDel = null;
        private static PatternMatchDel patternMatchDel = null;
        private static CompareFieldsValidDel compareFieldsValidDel = null;

        public static RequiredValidDel RequiredValidDel
        {
            get
            {
                if (requiredValidDel == null)
                    requiredValidDel = new RequiredValidDel(RequiredFieldValid);

                return requiredValidDel;
            }
        }

        public static StringLengValidDel StringLengValidDel
        {
            get
            {
                if (stringLengValidDel == null)
                    stringLengValidDel = new StringLengValidDel(StringLengValid);

                return stringLengValidDel;
            }
        }

        public static DateValidDel DateValidDel
        {
            get
            {
                if (dateValidDel == null)
                    dateValidDel = new DateValidDel(IsDateValid);

                return dateValidDel;
            }
        }

        public static PatternMatchDel PatternMatchDel
        {
            get
            {
                if (patternMatchDel == null)
                    patternMatchDel = new PatternMatchDel(PatternMatch);

                return patternMatchDel;
            }
        }

        public static CompareFieldsValidDel CompareFieldsValidDel
        {
            get
            {
                if (compareFieldsValidDel == null)
                    compareFieldsValidDel = new CompareFieldsValidDel(AreFieldsEqual);

                return compareFieldsValidDel;
            }
        }

        private static bool RequiredFieldValid(string fieldVal)
        {
            if (!string.IsNullOrEmpty(fieldVal))
                return true;

            return false;
        }

        private static bool StringLengValid(string fieldVal, int min, int max)
        {
            if (fieldVal.Length >= min && fieldVal.Length <= max) return true;

            return false;
        }

        private static bool IsDateValid(string fieldVal, out DateTime validDateTime)
        {
            if (DateTime.TryParse(fieldVal, out validDateTime))
                return true;

            return false;
        }

        private static bool PatternMatch(string fieldVal, string pattern)
        {
            Regex regex = new Regex(pattern);

            if (regex.IsMatch(fieldVal)) return true;

            return false;
        }

        private static bool AreFieldsEqual(string fieldVal, string fieldValCompare)
        {
            if (fieldVal.Equals(fieldValCompare)) return true;

            return false;
        }
    }
}
