﻿using System;
namespace ClubMembershipApplication.FieldValidators
{
    public interface IFieldValidator
    {
        public delegate bool FieldValidatorDel(int fieldIndex, string fieldValue, string[] fieldArray, out string fieldInvalidMessage);

        void InitiazeValidatorDelegates();
        string[] FieldArray { get; }
        FieldValidatorDel ValidatorDel { get; }
    }
}
