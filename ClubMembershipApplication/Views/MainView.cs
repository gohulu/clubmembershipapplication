﻿using System;
using ClubMembershipApplication.FieldValidators;

namespace ClubMembershipApplication.Views
{
    public class MainView : IView
    {
        public IFieldValidator FieldValidator => null;

        IView registerView = null;
        IView loginView = null;

        public MainView(IView registerView, IView loginView)
        {
            this.registerView = registerView;
            this.loginView = loginView;
        }

        public void RunView()
        {
            CommonOutputText.WriteMainHeading();

            Console.WriteLine("Please press 'l' to login or if you are not yet registered please press 'r'");

            ConsoleKey key = Console.ReadKey().Key;

            if (key == ConsoleKey.R)
            {
                RunUserRegistrationView();
                RunLoginView();
                return;
            }

            if (key == ConsoleKey.L)
            {
                RunLoginView();
                return;
            }

            Console.Clear();
            Console.WriteLine("Goodbye");
            Console.ReadKey();

        }

        private void RunUserRegistrationView()
        {
            this.registerView.RunView();
        }

        private void RunLoginView()
        {
            this.loginView.RunView();
        }
    }
}
